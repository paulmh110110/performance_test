﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.IO;
using System.Diagnostics;




namespace Perfomance_test
{
    /// <summary>
    /// Summary description for MS_PERF_21 - Login to Mattersphere.
    /// </summary>
    [CodedUITest]
    public class MS_PERF_21
    {
        public static void Log(string message)
        {
           
            var filename = "MS_PERF_21_results";
            var output = DateTime.Now.ToString() + " " + message + Environment.NewLine;

            File.AppendAllText(filename, output);
        }


        public MS_PERF_21()
        {
        }

        [TestMethod]
        public void MSPERF21()
        {
            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            Stopwatch sw = new Stopwatch();
            
            //open word, connect to UAT
            this.UIMap.openword();

            //click Login button,open command centre 
            //start stopwatch
            sw.Start();
            this.UIMap.loginandopencommandcentre();

            //stop stopwatch
            sw.Stop();
            //close command centre
            this.UIMap.closecommandcentre();

            var message = string.Format("The application took {0} seconds to run.", sw.Elapsed);
            Console.WriteLine(message);
            Log(message);
            
            //disconnect form mattersphere & close word
            this.UIMap.disconnectthencloseword();

        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
       
    }
}
