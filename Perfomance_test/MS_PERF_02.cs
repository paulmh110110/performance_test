﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Diagnostics;

namespace Perfomance_test
{
    /// <summary>
    /// Summary description for MS_PERF_02 - Opening a Word Document from the Documents tab when Word is not already open

    /// </summary>
    [CodedUITest]
    public class MS_PERF_02
    {
        public MS_PERF_02()
        {
        }

        [TestMethod]
        public void MSPERF02()
        {
            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            Stopwatch sw1 = new Stopwatch();

            //open excel, connect to UAT open command centre
            this.UIMap.opnexcelconnectuatopncs();

            //open matter UM0159312
            this.UIMap.opnmatterimportworddoc();

            //start stopwatch open recent matter
            sw1.Start();
            //open word doc & verify
            this.UIMap.opnworddoc();
           
            //stop stopwatch
            sw1.Stop();

            //close word doc
            this.UIMap.closeworddoc();

            //delete word doc
            this.UIMap.deleteworddoc();

            //close command centre
            this.UIMap.closecommandcentre003();

            //disconnect form mattersphere & close word
            this.UIMap.disconnectcloseword001();

            var message = string.Format("The application took {0} seconds to run.", sw1.Elapsed);
            Console.WriteLine(message);
            //Log(message);


        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
