﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.IO;
using System.Diagnostics;

namespace Perfomance_test
{
    /// <summary>
    /// Summary description for CMS_PERF_22 - Opening Command Centre
    /// </summary>
    [CodedUITest]
    public class MS_PERF_22
    {
        public MS_PERF_22()
        {
        }

        [TestMethod]
        public void MSPERF22()
        {
            Playback.PlaybackSettings.LoggerOverrideState = HtmlLoggerState.AllActionSnapshot;

            Stopwatch sw1 = new Stopwatch();

            //open word, connect to UAT click mattersphere
            this.UIMap.openwordconnecttouat001();

            //start stopwatch 
            sw1.Start();
            this.UIMap.opencommandcentre();

            //stop stopwatch
            sw1.Stop();
            //close command centre
            this.UIMap.closecommandcentre002();

            //disconnect form mattersphere & close word
            this.UIMap.disconnectthencloseword001();

            var message = string.Format("The application took {0} seconds to run.", sw1.Elapsed);
            Console.WriteLine(message);
            //Log(message);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
